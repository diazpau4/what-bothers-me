using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleChange : MonoBehaviour
{
    int index;
    public List<CircleCollider2D> _lstColliders = new List<CircleCollider2D>();

    private void Start()
    {
        foreach (Collider2D item in _lstColliders)
        {
            item.enabled = false;
        }
        index = 0;
        _lstColliders[0].enabled = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            _lstColliders[index].enabled = false;
            index++;
            if (index >= _lstColliders.Count)
            {
                index = 0;
            }
            _lstColliders[index].enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            _lstColliders[index].enabled = false;
            index--;
            if (index < 0)
            {
                index = _lstColliders.Count - 1;
            }
            _lstColliders[index].enabled = true;
        }

    }

    public bool Contador()
    {
        int contador = 0;
        foreach (Collider2D item in _lstColliders)
        {
            if (item.GetComponent<RotateObj>().enPos)
            {
                contador++;
            }
        }

        if (contador >= 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
