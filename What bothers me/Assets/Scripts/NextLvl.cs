using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLvl : MonoBehaviour
{

    public CircleChange circle;
    public string NombreEscena;

    public void Click()
    {
        if (circle.Contador())
        {
            SceneManager.LoadScene(NombreEscena);
        }
    }
    void Start()
    {
        circle = GetComponent<CircleChange>();
    }

    
    void Update()
    {
        
    }

    
}
